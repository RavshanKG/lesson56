import React, { Component } from 'react';
import './App.css';
import Burger from "../components/Burger/Burger";
import Calculator from "../components/Calculator/Calculator";

const price = {
  Salad: 5,
  Bacon: 30,
  Cheese: 20,
  Meat: 50
};

class App extends Component {
  state = {
    stuffing: {
      Salad: [],
      Bacon: [],
      Cheese: [],
      Meat: []
    }
  };

  addStuffing = (name) => {
    let stuffing = {...this.state.stuffing};
    let ingredient = [...stuffing[name]];

    ingredient.push(price[name]);
    stuffing[name] = ingredient;

    this.setState({stuffing});
  };

  removeStuffing = (name, className) => {
    if (className === 'Btn Less Disable') return null;

    let stuffing = {...this.state.stuffing};
    let ingredient = [...stuffing[name]];

    ingredient.splice(0, 1);
    stuffing[name] = ingredient;

    this.setState({stuffing});
  };

  render() {
    return (
      <div className="App">
        <Burger stuffing={this.state.stuffing}/>
        <Calculator
          stuffing={this.state.stuffing}
          price={price}
          less={this.removeStuffing}
          more={this.addStuffing}
        />
      </div>
    );
  }
}

export default App;
import React from 'react';
import './Burger.css';
import BreadTop from "./Stuffing/BreadTop";
import BreadBottom from "./Stuffing/BreadBottom";
import Stuffing from "./Stuffing/Stuffing";

const Burger = props => {
  return (
    <div className="Burger">
      <BreadTop />
      <Stuffing stuffing={props.stuffing}/>
      <BreadBottom />
    </div>
  );
};

export default Burger;
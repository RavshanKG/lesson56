import React from 'react';
import './Adjustment.css';
import Button from "./Button/Button";

const Adjustment = props => {
  let stuffing = (stuffing) => {
    return Object.keys(stuffing).map((key) => {
      let className = 'Btn Less';

      if (stuffing[key].length === 0) className += ' Disable';

      return (
        <div className="Ingredient" key={key}>
          <span>{key}</span>
          <Button className={"Btn More"} name={"More"} click={props.more} ingredient={key} />
          <Button className={className} name={"Less"} click={props.less} ingredient={key} />
        </div>
      );
    });
  };

  return (
    <div className="Adjustment">
      {stuffing(props.stuffing)}
    </div>
  );
};

export default Adjustment;